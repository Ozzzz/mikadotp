﻿using System.Data.SQLite;
using System.Diagnostics;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MikadoApp_TU.Models;

namespace UnitTestDemo.Tests
{
    public static class ContextFactory
    {
        public static SqliteConnection CreateDbFactory()
        {
            return new SqliteConnection("DataSource=:memory:");
        }

        public static MikadoContext CreateContext(this SqliteConnection connection)
        {
            connection.Open();

            var dbOptionBuilder = new DbContextOptionsBuilder<MikadoContext>();

            var methodInfos = new StackTrace().GetFrame(1).GetMethod();
            var className = methodInfos.ReflectedType.ReflectedType != null ? methodInfos.ReflectedType.ReflectedType.Name : methodInfos.ReflectedType.Name;

            dbOptionBuilder.UseSqlite(connection);

            var context = new MikadoContext(dbOptionBuilder.Options);

            context.Database.EnsureCreated();

            return context;
        }

        public static void InitialSeed(this MikadoContext context)
        {

        }
    }
}

