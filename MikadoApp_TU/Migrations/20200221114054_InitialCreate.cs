﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MikadoApp_TU.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Medias",
                columns: table => new
                {
                    Id_Media = table.Column<Guid>(nullable: false),
                    Type_Media = table.Column<string>(nullable: true),
                    Title_Media = table.Column<string>(nullable: true),
                    Author_Media = table.Column<string>(nullable: true),
                    MediaId_Media = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medias", x => x.Id_Media);
                    table.ForeignKey(
                        name: "FK_Medias_Medias_MediaId_Media",
                        column: x => x.MediaId_Media,
                        principalTable: "Medias",
                        principalColumn: "Id_Media",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID_User = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name_User = table.Column<string>(nullable: true),
                    Pwd_User = table.Column<string>(nullable: true),
                    Type_User = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID_User);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Medias_MediaId_Media",
                table: "Medias",
                column: "MediaId_Media");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Medias");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
