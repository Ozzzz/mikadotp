﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace MikadoApp_TU.Models
{
    public class MikadoContext : DbContext
    {
        public MikadoContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<Media> Medias { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=mikado.db");
    }
}
