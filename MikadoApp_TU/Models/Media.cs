﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MikadoApp_TU.Models
{
    public class Media
    {
        [Key]
        public Guid Id_Media { get; set; }
        public string Type_Media { get; set; }
        public string Title_Media { get; set; }
        public string Author_Media { get; set; }
        public List<Media> Favorites { get; set; }
    }
}
