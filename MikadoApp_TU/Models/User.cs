﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MikadoApp_TU.Models
{
    public class User
    {
        [Key]
        public int ID_User { get; set; }
        public string Name_User { get; set; }
        public string Pwd_User { get; set; }
        public string Type_User { get; set; }
    }
}
